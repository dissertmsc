PRINCIPAL= mscMonografia
LATEX_BIN= pdflatex
TEXTOS= $(PRINCIPAL).tex
XFIGFIGURAS=
DIAFIGURAS=
SLIDES=

.PHONY : pdf slides clean

pdf: $(PRINCIPAL).bbl
	$(LATEX_BIN) $(TEXTOS); $(LATEX_BIN) $(TEXTOS)

$(PRINCIPAL).aux:
	$(LATEX_BIN) $(TEXTOS)

$(PRINCIPAL).bbl : $(PRINCIPAL).aux
	bibtex $(PRINCIPAL)

clean :
	rm -f `tail -n +2 .gitignore`
